package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    WineDbHelper db;
    ListView listWine;
    List<Wine> wines = new ArrayList<Wine>();
    SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.listWine = findViewById(R.id.listWine);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, WineActivity.class);
//                myIntent.putExtra("wine", value);
                MainActivity.this.startActivity(myIntent);

            }
        });

        listWine.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Wine wine = wines.get(position);

                Intent myIntent = new Intent(MainActivity.this, WineActivity.class);
                myIntent.putExtra("Wine", wine);
                MainActivity.this.startActivity(myIntent);
            }
        });

        registerForContextMenu(listWine);
//        listWine.setOnCreateContextMenuListener();

        db = new WineDbHelper(this);
//        db.populate();
        this.initList();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu_main, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()) {
            case R.id.delete_id:

                Wine wine = wines.get(info.position);
                this.db.deleteWine(wine.getTitle(),wine.getRegion());

                this.initList();

                wines.remove(info.position);
                adapter.notifyDataSetChanged();

                Log.d("supprimer","supprimer");

            default:
                return super.onContextItemSelected(item);
        }
    }

    private void initList() {
        List<String> winesNames = new ArrayList<String>();

        Cursor cursor = db.fetchAllWines();

        // Parse the cursor
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Wine wine = db.cursorToWine(cursor);
                winesNames.add(wine.getTitle());
                wines.add(wine);
                cursor.moveToNext();
            }
        }

//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, winesNames);
        adapter = new SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[] { WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION },
                new int[] { android.R.id.text1,android.R.id.text2 }
        );

        this.listWine.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
