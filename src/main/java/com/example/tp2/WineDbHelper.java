package com.example.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 2;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";

    /**
     * Appellation
     */
    public static final String COLUMN_NAME = "name";

    /**
     * Region
     */
    public static final String COLUMN_WINE_REGION = "region";

    /**
     * Département
     */
    public static final String COLUMN_LOC = "localization";

    /**
     * Climat
     */
    public static final String COLUMN_CLIMATE = "climate";

    /**
     * Superficie
     */
    public static final String COLUMN_PLANTED_AREA = "publisher";

    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query = String.format(
            "CREATE TABLE %s (" +
                "%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                "%s TEXT," +
                "%s TEXT," +
                "%s TEXT," +
                "%s TEXT," +
                "%s INTEGER," +
                "UNIQUE (" + COLUMN_NAME + "," + COLUMN_WINE_REGION + ") ON CONFLICT ROLLBACK" +
            ")",
            TABLE_NAME,
            _ID,
            COLUMN_NAME,
            COLUMN_WINE_REGION,
            COLUMN_LOC,
            COLUMN_CLIMATE,
            COLUMN_PLANTED_AREA
        );
        Log.d("onCreate", query);

        // db.execSQL("SELECT * FROM " + TABLE_NAME);

        db.execSQL(query);

        Log.d("execSQL", "DONE");

	    // db.execSQL() with the CREATE TABLE ... command
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("onUpgrade", "onUpgrade");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.close();
        onCreate(db);
    }


   /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine) {

        SQLiteDatabase db = this.getWritableDatabase();

        String sql = String.format(
            "SELECT * FROM %s WHERE name = '%s' and region = '%s'",
            TABLE_NAME,
            wine.getTitle(),
            wine.getRegion()
        );
        Cursor c = db.rawQuery(sql, null);

        long rowID;

        if (c.getCount() == 0) {

            ContentValues wineValues = new ContentValues();
            wineValues.put(COLUMN_NAME,wine.getTitle());
            wineValues.put(COLUMN_WINE_REGION,wine.getRegion());
            wineValues.put(COLUMN_LOC,wine.getLocalization());
            wineValues.put(COLUMN_CLIMATE,wine.getClimate());
            wineValues.put(COLUMN_PLANTED_AREA,wine.getPlantedArea());

            rowID = db.insert(TABLE_NAME,null,wineValues);

        } else {
            rowID = -1;
        }

        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        int res;

        ContentValues wineValues = new ContentValues();
        wineValues.put(_ID,wine.getId());
        wineValues.put(COLUMN_NAME,wine.getTitle());
        wineValues.put(COLUMN_WINE_REGION,wine.getRegion());
        wineValues.put(COLUMN_LOC,wine.getLocalization());
        wineValues.put(COLUMN_CLIMATE,wine.getClimate());
        wineValues.put(COLUMN_PLANTED_AREA,wine.getPlantedArea());

        res = db.update(TABLE_NAME,wineValues, _ID + "=" + wine.getId(), null);
        db.close();

        return res;
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor;

        String sql = String.format(
            "SELECT * FROM %s",
            TABLE_NAME
        );
        cursor = db.rawQuery(sql, null);
        
        if (cursor != null) {
            cursor.moveToFirst();
        }

        db.close();
        return cursor;
    }

    public void deleteWine(String name, String region) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME,COLUMN_NAME + "= \"" + name + "\" AND " + COLUMN_WINE_REGION + "= \"" + region + "\"", null);
        db.close();
    }

     public void populate() {

        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d("populate", "nb of rows="+numRows);
        db.close();
    }

    public static Wine cursorToWine(Cursor cursor) {
        Wine wine;

        // build a Wine object from cursor
        wine = new Wine(
            cursor.getLong(cursor.getColumnIndex(WineDbHelper._ID)),
            cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_NAME)),
            cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_WINE_REGION)),
            cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_LOC)),
            cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_CLIMATE)),
            cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_PLANTED_AREA))
        );

        return wine;
    }
}
